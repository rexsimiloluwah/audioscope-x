import React, { useEffect, useRef, useState } from "react";
import style from "./index.module.css";

const ToneVisualizer: React.FC = () => {
  const [oscillatorType, setOscillatorType] = useState<OscillatorType>("sine");
  const [frequency, setFrequency] = useState<number>(440);
  const [inputGain, setInputGain] = useState<number>(0.5);
  const [oscillator, setOscillator] = useState<OscillatorNode | null>(null);
  const [gainNode, setGainNode] = useState<GainNode | null>(null);
  const [isPlaying, setIsPlaying] = useState<boolean>(false);
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const audioCtx = new AudioContext();
  let shouldAnimate: boolean = false;

  let analyzer = new AnalyserNode(audioCtx, {
    smoothingTimeConstant: 1,
    fftSize: 2048,
  });

  const dataArray = new Uint8Array(analyzer.frequencyBinCount);

  const handleStart = () => {
    // Create an oscillator node for the audio context
    const newOscillator = new OscillatorNode(audioCtx, {
      type: oscillatorType,
      frequency: frequency,
    });
    const newGainNode = new GainNode(audioCtx, {
      gain: 0.5,
    });
    setOscillator(newOscillator);
    setGainNode(newGainNode);
    let now = audioCtx.currentTime;
    newGainNode.gain.setValueAtTime(inputGain, now);
    //newOscillator.frequency.setValueAtTime(440, audioCtx.currentTime);
    newOscillator.connect(newGainNode);
    newGainNode.connect(analyzer);
    analyzer.connect(audioCtx.destination);
    newOscillator.start(now);
    shouldAnimate = true;
    setIsPlaying(true);
    draw();
  };

  const draw = () => {
    if (canvasRef.current) {
      console.log("drawing!");
      const c = canvasRef.current.getContext("2d") as CanvasRenderingContext2D;
      const canvasW = canvasRef.current.width; // canvas width
      const canvasH = canvasRef.current.height; // canvas height

      // populates the data array with time domain data points
      analyzer.getByteTimeDomainData(dataArray);

      // compute the width of each segment
      // width of canvas/frequencyBinCount
      const segmentWidth = canvasW / analyzer.frequencyBinCount; // frequency bin count determines the number of data points
      c.fillRect(0, 0, canvasW, canvasH);

      // draw the grids
      drawGrids(canvasRef.current);

      c.strokeStyle = "green";
      c.lineWidth = 2;
      c.beginPath();

      // visualize the data
      for (let i = 1; i < analyzer.frequencyBinCount; i += 1) {
        let x = i * segmentWidth;
        let v = dataArray[i] / 256.0;
        let y = (v * canvasH) / 2;
        if (i === 0) {
          c.moveTo(x, y + 50);
        } else {
          c.lineTo(x, y + 50);
        }
      }
      c.stroke();
      // loop the visualization
      if (shouldAnimate) {
        requestAnimationFrame(draw);
      } else {
        initCanvas(canvasRef.current);
      }
    }
  };

  const handleStop = () => {
    shouldAnimate = false;
    oscillator?.stop();
    setIsPlaying(false);
    // temporary: until I figure out how to fix this animation frame bug
    window.location.reload();
  };

  const initCanvas = (canvas: HTMLCanvasElement) => {
    const canvasW = canvas.width; // Canvas width
    const canvasH = canvas.height; // Canvas height
    const c = canvas.getContext("2d") as CanvasRenderingContext2D;

    // clear the canvas
    c.clearRect(0, 0, canvasW, canvasH);

    console.log(canvasW, canvasH);
    // for sharper strokes
    const sizeOnScreen = canvas.getBoundingClientRect();
    const pixelRatio = window.devicePixelRatio;
    canvas.width = Math.floor(sizeOnScreen.width * pixelRatio);
    canvas.height = Math.floor(sizeOnScreen.height * pixelRatio);

    // styling
    c.fillStyle = "#181818";
    c.fillRect(0, 0, canvasW, canvasH);
    c.strokeStyle = "green";
    c.lineWidth = 2;

    // Draw a straight line in the middle of the canvas
    c.beginPath();
    c.moveTo(0, canvasH / 2); // move to the middle of the canvas
    c.lineTo(canvasW, canvasH / 2); // line covers the entire width
    c.stroke();

    drawGrids(canvasRef.current as HTMLCanvasElement);
  };

  const drawGrids = (canvas: HTMLCanvasElement) => {
    const canvasW = canvas.width;
    const canvasH = canvas.height;
    const c = canvas.getContext("2d") as CanvasRenderingContext2D;
    for (let i = 0; i < canvasW; i += canvasW / 50) {
      c.strokeStyle = "#eee";
      c.lineWidth = 0.2;
      c.beginPath();
      c.moveTo(i, 0);
      c.lineTo(i, canvasH);
      c.stroke();
    }
    for (let i = 0; i < canvasH; i += canvasW / 50) {
      c.strokeStyle = "#eee";
      c.lineWidth = 0.2;
      c.beginPath();
      c.moveTo(0, i);
      c.lineTo(canvasW, i);
      c.stroke();
    }
  };

  // initialize the canvas
  useEffect(() => {
    if (canvasRef.current) {
      initCanvas(canvasRef.current);
      initCanvas(canvasRef.current); // bad code
    }
  });

  // respond to change events
  useEffect(() => {
    if (oscillator && gainNode) {
      gainNode.gain.setValueAtTime(inputGain, audioCtx.currentTime);
      oscillator.type = oscillatorType;
      oscillator.frequency.setValueAtTime(frequency, audioCtx.currentTime); // value in hertz
    }
  }, [
    frequency,
    inputGain,
    audioCtx.currentTime,
    gainNode,
    oscillatorType,
    oscillator,
  ]);

  return (
    <div>
      <div className={style.control__panel}>
        <button
          type="button"
          className={style.power__button}
          onClick={isPlaying ? handleStop : handleStart}
        >
          {isPlaying ? "Stop" : "Start"}
        </button>
        <div className={style.form__row}>
          <label htmlFor="oscillator-type">Oscillator Type</label>
          <select
            name="oscillator-type"
            id="oscillator-type"
            onChange={(e) =>
              setOscillatorType(e.target.value as OscillatorType)
            }
          >
            <option value="sine">Sine</option>
            <option value="sawtooth">Sawtooth</option>
            <option value="triangle">Triangle</option>
            <option value="square">Square</option>
          </select>
        </div>
        <div className={style.form__row}>
          <label htmlFor="frequency">Frequency: {frequency}Hz</label>
          <input
            id="frequency"
            type="range"
            min={110}
            max={1760}
            step={1}
            value={frequency}
            onChange={(e) => {
              setFrequency(Number(e.target.value));
            }}
          />
        </div>
        <div className={style.form__row}>
          <label htmlFor="input__gain">Gain: {inputGain} (scaled)</label>
          <input
            id="input__gain"
            type="range"
            min={0}
            max={1}
            step={0.05}
            value={inputGain}
            onChange={(e) => {
              setInputGain(Number(e.target.value));
            }}
          />
        </div>
      </div>

      <canvas id="canvas" className={style.canvas} ref={canvasRef}></canvas>
    </div>
  );
};

export default ToneVisualizer;
