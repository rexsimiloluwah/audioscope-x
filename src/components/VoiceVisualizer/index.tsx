import React, { useRef, useState, useEffect } from "react";
import style from "./index.module.css";

const VoiceVisualizer: React.FC = () => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [isRecording, setIsRecording] = useState<boolean>(false);
  const [recorder, setRecorder] = useState<MediaRecorder | null>(null);
  let audioCtx: AudioContext;
  let shouldAnimate: boolean;

  const startRecording = async () => {
    const constraints = { audio: true };
    let recordingChunks: BlobPart[] = [];
    const onSuccess = (stream: MediaStream) => {
      // initialize the recorder
      const newRecorder = new MediaRecorder(stream);
      setRecorder(newRecorder);
      newRecorder.start(1000);
      setIsRecording(true);
      shouldAnimate = true;
      // visualize the recording stream
      visualizeMediaStream(stream);

      newRecorder.ondataavailable = (e: BlobEvent) => {
        recordingChunks.push(e.data);
      };

      newRecorder.onstop = () => {
        shouldAnimate = false;
        const blob = new Blob(recordingChunks as BlobPart[], {
          type: "audio/ogg; codecs=opus",
        });
        // clear the recording chunks
        const audioURL = window.URL.createObjectURL(blob);
        console.log(audioURL);
        let reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onloadend = () => {
          console.log(reader.result);
        };
      };
    };

    const onFailure = () => {
      alert("Browser does not support media recording!");
    };

    navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onFailure);
  };

  const stopRecording = async () => {
    recorder?.stop();
    setIsRecording(false);
    if (canvasRef.current) {
      initCanvas(canvasRef.current);
    }
  };

  const visualizeMediaStream = (stream: MediaStream) => {
    // initialize the audio context
    if (!audioCtx) {
      audioCtx = new AudioContext();
    }

    // create the media stream source
    const source = audioCtx.createMediaStreamSource(stream);

    // initialize the analyzer
    let analyzer = new AnalyserNode(audioCtx, {
      smoothingTimeConstant: 1,
      fftSize: 2048,
    });
    const bufferLength = analyzer.frequencyBinCount;
    const dataArray = new Uint8Array(bufferLength);

    // connect the media stream source to the analyzer
    source.connect(analyzer);

    const draw = () => {
      if (canvasRef.current) {
        const c = canvasRef.current.getContext(
          "2d"
        ) as CanvasRenderingContext2D;
        const canvasW = canvasRef.current.width; // canvas width
        const canvasH = canvasRef.current.height; // canvas height

        // populates the data array with the time domain data points
        analyzer.getByteTimeDomainData(dataArray);

        c.fillStyle = "#181818";
        c.fillRect(0, 0, canvasW, canvasH);

        drawGrids(canvasRef.current);

        c.lineWidth = 2;
        c.strokeStyle = "green";

        c.beginPath();

        // compute the width of each segment
        // width of the canvas/ buffer length
        let segmentWidth = (canvasW * 1.0) / bufferLength;

        // visualize the data
        for (let i = 0; i < bufferLength; i++) {
          let x = i * segmentWidth;
          let v = dataArray[i] / 128.0;
          let y = (v * canvasH) / 2;

          if (i === 0) {
            c.moveTo(x, y);
          } else {
            c.lineTo(x, y);
          }
        }

        c.lineTo(canvasW, canvasH / 2);
        c.stroke();
        // loop the visualization
        if (shouldAnimate) {
          requestAnimationFrame(draw);
        } else {
          initCanvas(canvasRef.current);
        }
      }
    };

    draw();
  };

  const drawGrids = (canvas: HTMLCanvasElement) => {
    const canvasW = canvas.width;
    const canvasH = canvas.height;
    const c = canvas.getContext("2d") as CanvasRenderingContext2D;
    for (let i = 0; i < canvasW; i += canvasW / 50) {
      c.strokeStyle = "#eee";
      c.lineWidth = 0.2;
      c.beginPath();
      c.moveTo(i, 0);
      c.lineTo(i, canvasH);
      c.stroke();
    }
    for (let i = 0; i < canvasH; i += canvasW / 50) {
      c.strokeStyle = "#eee";
      c.lineWidth = 0.2;
      c.beginPath();
      c.moveTo(0, i);
      c.lineTo(canvasW, i);
      c.stroke();
    }
  };

  const initCanvas = (canvas: HTMLCanvasElement) => {
    console.log("init canvas");
    const pixelRatio = window.devicePixelRatio;
    const canvasW = canvas.width; // Canvas width
    const canvasH = canvas.height; // Canvas height
    const c = canvas.getContext("2d") as CanvasRenderingContext2D;

    // clear the canvas
    c.clearRect(0, 0, canvasW, canvasH);

    // for sharper strokes
    const sizeOnScreen = canvas.getBoundingClientRect();
    canvas.width = sizeOnScreen.width * pixelRatio;
    canvas.height = sizeOnScreen.height * pixelRatio;

    // styling
    c.fillStyle = "#181818";
    c.fillRect(0, 0, canvasW, canvasH);
    c.strokeStyle = "green";
    c.lineWidth = 2;

    // Draw a straight line in the middle of the canvas
    c.beginPath();
    c.moveTo(0, canvasH / 2); // move to the middle of the canvas
    c.lineTo(canvasW, canvasH / 2); // line covers the entire width
    c.stroke();

    drawGrids(canvas);
  };

  // initialize the canvas
  useEffect(() => {
    if (canvasRef.current) {
      initCanvas(canvasRef.current);
      initCanvas(canvasRef.current); // bad code
    }
  });

  return (
    <div>
      <div className={style.control__panel}>
        {isRecording ? (
          <button className={style.listening__button}>
            Listening{" "}
            <svg
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M11 17a1 1 0 001.447.894l4-2A1 1 0 0017 15V9.236a1 1 0 00-1.447-.894l-4 2a1 1 0 00-.553.894V17zM15.211 6.276a1 1 0 000-1.788l-4.764-2.382a1 1 0 00-.894 0L4.789 4.488a1 1 0 000 1.788l4.764 2.382a1 1 0 00.894 0l4.764-2.382zM4.447 8.342A1 1 0 003 9.236V15a1 1 0 00.553.894l4 2A1 1 0 009 17v-5.764a1 1 0 00-.553-.894l-4-2z" />
            </svg>
          </button>
        ) : (
          <button onClick={startRecording} className={style.start__button}>
            Start Recording
          </button>
        )}
        {isRecording && (
          <button onClick={stopRecording} className={style.stop__button}>
            Stop
          </button>
        )}
      </div>
      <canvas id="canvas" className={style.canvas} ref={canvasRef}></canvas>
    </div>
  );
};

export default VoiceVisualizer;
