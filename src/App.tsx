import React, { useState } from "react";
import { ToneVisualizer, VoiceVisualizer } from "./components";

function App() {
  const [activeMode, setActiveMode] = useState<number>(0);

  const modes = [
    { label: "Tone Visualizer", component: <ToneVisualizer /> },
    { label: "Voice Visualizer", component: <VoiceVisualizer /> },
  ];
  return (
    <div>
      <div className="main">
        <div className="mode__buttons">
          {modes.map((item, i) => (
            <button
              type="button"
              className={`${activeMode === i && "active"}`}
              onClick={() => setActiveMode(i)}
              key={i}
            >
              {item.label}
            </button>
          ))}
        </div>
        <div className="visualizer">{modes[activeMode].component}</div>
      </div>
    </div>
  );
}

export default App;
